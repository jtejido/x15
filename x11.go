// Use of this source code is governed by an ISC
// license that can be found in the LICENSE file.

package x11

import (
	"bitbucket.org/jtejido/x15/blake"
	"bitbucket.org/jtejido/x15/bmw"
	"bitbucket.org/jtejido/x15/cubed"
	"bitbucket.org/jtejido/x15/echo"
	"bitbucket.org/jtejido/x15/groest"
	"bitbucket.org/jtejido/x15/hash"
	"bitbucket.org/jtejido/x15/jhash"
	"bitbucket.org/jtejido/x15/keccak"
	"bitbucket.org/jtejido/x15/luffa"
	"bitbucket.org/jtejido/x15/shavite"
	"bitbucket.org/jtejido/x15/simd"
	"bitbucket.org/jtejido/x15/skein"
	"bitbucket.org/jtejido/x15/hamsi"
)

type Hash struct {
	tha [64]byte
	thb [64]byte

	blake   hash.Digest
	bmw     hash.Digest
	cubed   hash.Digest
	echo    hash.Digest
	groest  hash.Digest
	jhash   hash.Digest
	keccak  hash.Digest
	luffa   hash.Digest
	shavite hash.Digest
	simd    hash.Digest
	skein   hash.Digest
	hamsi   hash.Digest
}

// New returns a new object to compute a x11 hash.
func New() *Hash {
	ref := &Hash{}
	ref.blake = blake.New()
	ref.bmw = bmw.New()
	ref.cubed = cubed.New()
	ref.echo = echo.New()
	ref.groest = groest.New()
	ref.jhash = jhash.New()
	ref.keccak = keccak.New()
	ref.luffa = luffa.New()
	ref.shavite = shavite.New()
	ref.simd = simd.New()
	ref.skein = skein.New()
	ref.hamsi = hamsi.New()
	return ref
}

// Hash computes the hash from the src bytes and stores the result in dst.
func (ref *Hash) Hash(src []byte, dst []byte) {
	ta := ref.tha[:]
	tb := ref.thb[:]

	ref.blake.Write(src)
	ref.blake.Close(tb, 0, 0)

	ref.bmw.Write(tb)
	ref.bmw.Close(ta, 0, 0)

	ref.groest.Write(ta)
	ref.groest.Close(tb, 0, 0)

	ref.skein.Write(tb)
	ref.skein.Close(ta, 0, 0)

	ref.jhash.Write(ta)
	ref.jhash.Close(tb, 0, 0)

	ref.keccak.Write(tb)
	ref.keccak.Close(ta, 0, 0)

	ref.luffa.Write(ta)
	ref.luffa.Close(tb, 0, 0)

	ref.cubed.Write(tb)
	ref.cubed.Close(ta, 0, 0)

	ref.shavite.Write(ta)
	ref.shavite.Close(tb, 0, 0)

	ref.simd.Write(tb)
	ref.simd.Close(ta, 0, 0)

	ref.echo.Write(ta)
	ref.echo.Close(tb, 0, 0)

	ref.hamsi.Write(tb)
	ref.hamsi.Close(ta, 0, 0)

	copy(dst, tb)
}
