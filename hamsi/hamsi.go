package hamsi

import (
	// "fmt"
	"bitbucket.org/jtejido/x15/hash"
)

const HashSize = int(64)

const BlockSize = uintptr(32)

type digest struct {
	partial uint64
	partialLen int
	bitCount uint64
	h [16]uint64

	b [BlockSize]byte
}


var iv = [16]uint64{
	uint64(0x73746565), uint64(0x6c706172),
	uint64(0x6b204172), uint64(0x656e6265),
	uint64(0x72672031), uint64(0x302c2062),
	uint64(0x75732032), uint64(0x3434362c),
	uint64(0x20422d33), uint64(0x30303120),
	uint64(0x4c657576), uint64(0x656e2d48),
	uint64(0x65766572), uint64(0x6c65652c),
	uint64(0x2042656c), uint64(0x6769756d),
}

var tsrc = [64][16]uint64{
		{ uint64(0x466d0c00), uint64(0x08620000), uint64(0xdd5d0000), uint64(0xbadd0000),
		  uint64(0x6a927942), uint64(0x441f2b93), uint64(0x218ace6f), uint64(0xbf2c0be2),
		  uint64(0x6f299000), uint64(0x6c850000), uint64(0x2f160000), uint64(0x782e0000),
		  uint64(0x644c37cd), uint64(0x12dd1cd6), uint64(0xd26a8c36), uint64(0x32219526), },
		{ uint64(0x29449c00), uint64(0x64e70000), uint64(0xf24b0000), uint64(0xc2f30000),
		  uint64(0x0ede4e8f), uint64(0x56c23745), uint64(0xf3e04259), uint64(0x8d0d9ec4),
		  uint64(0x466d0c00), uint64(0x08620000), uint64(0xdd5d0000), uint64(0xbadd0000),
		  uint64(0x6a927942), uint64(0x441f2b93), uint64(0x218ace6f), uint64(0xbf2c0be2), },
		{ uint64(0x9cbb1800), uint64(0xb0d30000), uint64(0x92510000), uint64(0xed930000),
		  uint64(0x593a4345), uint64(0xe114d5f4), uint64(0x430633da), uint64(0x78cace29),
		  uint64(0xc8934400), uint64(0x5a3e0000), uint64(0x57870000), uint64(0x4c560000),
		  uint64(0xea982435), uint64(0x75b11115), uint64(0x28b67247), uint64(0x2dd1f9ab), },
		{ uint64(0x54285c00), uint64(0xeaed0000), uint64(0xc5d60000), uint64(0xa1c50000),
		  uint64(0xb3a26770), uint64(0x94a5c4e1), uint64(0x6bb0419d), uint64(0x551b3782),
		  uint64(0x9cbb1800), uint64(0xb0d30000), uint64(0x92510000), uint64(0xed930000),
		  uint64(0x593a4345), uint64(0xe114d5f4), uint64(0x430633da), uint64(0x78cace29), },
		{ uint64(0x23671400), uint64(0xc8b90000), uint64(0xf4c70000), uint64(0xfb750000),
		  uint64(0x73cd2465), uint64(0xf8a6a549), uint64(0x02c40a3f), uint64(0xdc24e61f),
		  uint64(0x373d2800), uint64(0x71500000), uint64(0x95e00000), uint64(0x0a140000),
		  uint64(0xbdac1909), uint64(0x48ef9831), uint64(0x456d6d1f), uint64(0x3daac2da), },
		{ uint64(0x145a3c00), uint64(0xb9e90000), uint64(0x61270000), uint64(0xf1610000),
		  uint64(0xce613d6c), uint64(0xb0493d78), uint64(0x47a96720), uint64(0xe18e24c5),
		  uint64(0x23671400), uint64(0xc8b90000), uint64(0xf4c70000), uint64(0xfb750000),
		  uint64(0x73cd2465), uint64(0xf8a6a549), uint64(0x02c40a3f), uint64(0xdc24e61f), },
		{ uint64(0xc96b0030), uint64(0xe7250000), uint64(0x2f840000), uint64(0x264f0000),
		  uint64(0x08695bf9), uint64(0x6dfcf137), uint64(0x509f6984), uint64(0x9e69af68),
		  uint64(0x26600240), uint64(0xddd80000), uint64(0x722a0000), uint64(0x4f060000),
		  uint64(0x936667ff), uint64(0x29f944ce), uint64(0x368b63d5), uint64(0x0c26f262), },
		{ uint64(0xef0b0270), uint64(0x3afd0000), uint64(0x5dae0000), uint64(0x69490000),
		  uint64(0x9b0f3c06), uint64(0x4405b5f9), uint64(0x66140a51), uint64(0x924f5d0a),
		  uint64(0xc96b0030), uint64(0xe7250000), uint64(0x2f840000), uint64(0x264f0000),
		  uint64(0x08695bf9), uint64(0x6dfcf137), uint64(0x509f6984), uint64(0x9e69af68), },
		{ uint64(0xb4370060), uint64(0x0c4c0000), uint64(0x56c20000), uint64(0x5cae0000),
		  uint64(0x94541f3f), uint64(0x3b3ef825), uint64(0x1b365f3d), uint64(0xf3d45758),
		  uint64(0x5cb00110), uint64(0x913e0000), uint64(0x44190000), uint64(0x888c0000),
		  uint64(0x66dc7418), uint64(0x921f1d66), uint64(0x55ceea25), uint64(0x925c44e9), },
		{ uint64(0xe8870170), uint64(0x9d720000), uint64(0x12db0000), uint64(0xd4220000),
		  uint64(0xf2886b27), uint64(0xa921e543), uint64(0x4ef8b518), uint64(0x618813b1),
		  uint64(0xb4370060), uint64(0x0c4c0000), uint64(0x56c20000), uint64(0x5cae0000),
		  uint64(0x94541f3f), uint64(0x3b3ef825), uint64(0x1b365f3d), uint64(0xf3d45758), },
		{ uint64(0xf46c0050), uint64(0x96180000), uint64(0x14a50000), uint64(0x031f0000),
		  uint64(0x42947eb8), uint64(0x66bf7e19), uint64(0x9ca470d2), uint64(0x8a341574),
		  uint64(0x832800a0), uint64(0x67420000), uint64(0xe1170000), uint64(0x370b0000),
		  uint64(0xcba30034), uint64(0x3c34923c), uint64(0x9767bdcc), uint64(0x450360bf), },
		{ uint64(0x774400f0), uint64(0xf15a0000), uint64(0xf5b20000), uint64(0x34140000),
		  uint64(0x89377e8c), uint64(0x5a8bec25), uint64(0x0bc3cd1e), uint64(0xcf3775cb),
		  uint64(0xf46c0050), uint64(0x96180000), uint64(0x14a50000), uint64(0x031f0000),
		  uint64(0x42947eb8), uint64(0x66bf7e19), uint64(0x9ca470d2), uint64(0x8a341574), },
		{ uint64(0xd46a0000), uint64(0x8dc8c000), uint64(0xa5af0000), uint64(0x4a290000),
		  uint64(0xfc4e427a), uint64(0xc9b4866c), uint64(0x98369604), uint64(0xf746c320),
		  uint64(0x231f0009), uint64(0x42f40000), uint64(0x66790000), uint64(0x4ebb0000),
		  uint64(0xfedb5bd3), uint64(0x315cb0d6), uint64(0xe2b1674a), uint64(0x69505b3a), },
		{ uint64(0xf7750009), uint64(0xcf3cc000), uint64(0xc3d60000), uint64(0x04920000),
		  uint64(0x029519a9), uint64(0xf8e836ba), uint64(0x7a87f14e), uint64(0x9e16981a),
		  uint64(0xd46a0000), uint64(0x8dc8c000), uint64(0xa5af0000), uint64(0x4a290000),
		  uint64(0xfc4e427a), uint64(0xc9b4866c), uint64(0x98369604), uint64(0xf746c320), },
		{ uint64(0xa67f0001), uint64(0x71378000), uint64(0x19fc0000), uint64(0x96db0000),
		  uint64(0x3a8b6dfd), uint64(0xebcaaef3), uint64(0x2c6d478f), uint64(0xac8e6c88),
		  uint64(0x50ff0004), uint64(0x45744000), uint64(0x3dfb0000), uint64(0x19e60000),
		  uint64(0x1bbc5606), uint64(0xe1727b5d), uint64(0xe1a8cc96), uint64(0x7b1bd6b9), },
		{ uint64(0xf6800005), uint64(0x3443c000), uint64(0x24070000), uint64(0x8f3d0000),
		  uint64(0x21373bfb), uint64(0x0ab8d5ae), uint64(0xcdc58b19), uint64(0xd795ba31),
		  uint64(0xa67f0001), uint64(0x71378000), uint64(0x19fc0000), uint64(0x96db0000),
		  uint64(0x3a8b6dfd), uint64(0xebcaaef3), uint64(0x2c6d478f), uint64(0xac8e6c88), },
		{ uint64(0xeecf0001), uint64(0x6f564000), uint64(0xf33e0000), uint64(0xa79e0000),
		  uint64(0xbdb57219), uint64(0xb711ebc5), uint64(0x4a3b40ba), uint64(0xfeabf254),
		  uint64(0x9b060002), uint64(0x61468000), uint64(0x221e0000), uint64(0x1d740000),
		  uint64(0x36715d27), uint64(0x30495c92), uint64(0xf11336a7), uint64(0xfe1cdc7f), },
		{ uint64(0x75c90003), uint64(0x0e10c000), uint64(0xd1200000), uint64(0xbaea0000),
		  uint64(0x8bc42f3e), uint64(0x8758b757), uint64(0xbb28761d), uint64(0x00b72e2b),
		  uint64(0xeecf0001), uint64(0x6f564000), uint64(0xf33e0000), uint64(0xa79e0000),
		  uint64(0xbdb57219), uint64(0xb711ebc5), uint64(0x4a3b40ba), uint64(0xfeabf254), },
		{ uint64(0xd1660000), uint64(0x1bbc0300), uint64(0x9eec0000), uint64(0xf6940000),
		  uint64(0x03024527), uint64(0xcf70fcf2), uint64(0xb4431b17), uint64(0x857f3c2b),
		  uint64(0xa4c20000), uint64(0xd9372400), uint64(0x0a480000), uint64(0x66610000),
		  uint64(0xf87a12c7), uint64(0x86bef75c), uint64(0xa324df94), uint64(0x2ba05a55), },
		{ uint64(0x75a40000), uint64(0xc28b2700), uint64(0x94a40000), uint64(0x90f50000),
		  uint64(0xfb7857e0), uint64(0x49ce0bae), uint64(0x1767c483), uint64(0xaedf667e),
		  uint64(0xd1660000), uint64(0x1bbc0300), uint64(0x9eec0000), uint64(0xf6940000),
		  uint64(0x03024527), uint64(0xcf70fcf2), uint64(0xb4431b17), uint64(0x857f3c2b), },
		{ uint64(0xb83d0000), uint64(0x16710600), uint64(0x379a0000), uint64(0xf5b10000),
		  uint64(0x228161ac), uint64(0xae48f145), uint64(0x66241616), uint64(0xc5c1eb3e),
		  uint64(0xfd250000), uint64(0xb3c41100), uint64(0xcef00000), uint64(0xcef90000),
		  uint64(0x3c4d7580), uint64(0x8d5b6493), uint64(0x7098b0a6), uint64(0x1af21fe1), },
		{ uint64(0x45180000), uint64(0xa5b51700), uint64(0xf96a0000), uint64(0x3b480000),
		  uint64(0x1ecc142c), uint64(0x231395d6), uint64(0x16bca6b0), uint64(0xdf33f4df),
		  uint64(0xb83d0000), uint64(0x16710600), uint64(0x379a0000), uint64(0xf5b10000),
		  uint64(0x228161ac), uint64(0xae48f145), uint64(0x66241616), uint64(0xc5c1eb3e), },
		{ uint64(0xfe220000), uint64(0xa7580500), uint64(0x25d10000), uint64(0xf7600000),
		  uint64(0x893178da), uint64(0x1fd4f860), uint64(0x4ed0a315), uint64(0xa123ff9f),
		  uint64(0xf2500000), uint64(0xeebd0a00), uint64(0x67a80000), uint64(0xab8a0000),
		  uint64(0xba9b48c0), uint64(0x0a56dd74), uint64(0xdb73e86e), uint64(0x1568ff0f), },
		{ uint64(0x0c720000), uint64(0x49e50f00), uint64(0x42790000), uint64(0x5cea0000),
		  uint64(0x33aa301a), uint64(0x15822514), uint64(0x95a34b7b), uint64(0xb44b0090),
		  uint64(0xfe220000), uint64(0xa7580500), uint64(0x25d10000), uint64(0xf7600000),
		  uint64(0x893178da), uint64(0x1fd4f860), uint64(0x4ed0a315), uint64(0xa123ff9f), },
		{ uint64(0xc6730000), uint64(0xaf8d000c), uint64(0xa4c10000), uint64(0x218d0000),
		  uint64(0x23111587), uint64(0x7913512f), uint64(0x1d28ac88), uint64(0x378dd173),
		  uint64(0xaf220000), uint64(0x7b6c0090), uint64(0x67e20000), uint64(0x8da20000),
		  uint64(0xc7841e29), uint64(0xb7b744f3), uint64(0x9ac484f4), uint64(0x8b6c72bd), },
		{ uint64(0x69510000), uint64(0xd4e1009c), uint64(0xc3230000), uint64(0xac2f0000),
		  uint64(0xe4950bae), uint64(0xcea415dc), uint64(0x87ec287c), uint64(0xbce1a3ce),
		  uint64(0xc6730000), uint64(0xaf8d000c), uint64(0xa4c10000), uint64(0x218d0000),
		  uint64(0x23111587), uint64(0x7913512f), uint64(0x1d28ac88), uint64(0x378dd173), },
		{ uint64(0xbc8d0000), uint64(0xfc3b0018), uint64(0x19830000), uint64(0xd10b0000),
		  uint64(0xae1878c4), uint64(0x42a69856), uint64(0x0012da37), uint64(0x2c3b504e),
		  uint64(0xe8dd0000), uint64(0xfa4a0044), uint64(0x3c2d0000), uint64(0xbb150000),
		  uint64(0x80bd361b), uint64(0x24e81d44), uint64(0xbfa8c2f4), uint64(0x524a0d59), },
		{ uint64(0x54500000), uint64(0x0671005c), uint64(0x25ae0000), uint64(0x6a1e0000),
		  uint64(0x2ea54edf), uint64(0x664e8512), uint64(0xbfba18c3), uint64(0x7e715d17),
		  uint64(0xbc8d0000), uint64(0xfc3b0018), uint64(0x19830000), uint64(0xd10b0000),
		  uint64(0xae1878c4), uint64(0x42a69856), uint64(0x0012da37), uint64(0x2c3b504e), },
		{ uint64(0xe3430000), uint64(0x3a4e0014), uint64(0xf2c60000), uint64(0xaa4e0000),
		  uint64(0xdb1e42a6), uint64(0x256bbe15), uint64(0x123db156), uint64(0x3a4e99d7),
		  uint64(0xf75a0000), uint64(0x19840028), uint64(0xa2190000), uint64(0xeef80000),
		  uint64(0xc0722516), uint64(0x19981260), uint64(0x73dba1e6), uint64(0xe1844257), },
		{ uint64(0x14190000), uint64(0x23ca003c), uint64(0x50df0000), uint64(0x44b60000),
		  uint64(0x1b6c67b0), uint64(0x3cf3ac75), uint64(0x61e610b0), uint64(0xdbcadb80),
		  uint64(0xe3430000), uint64(0x3a4e0014), uint64(0xf2c60000), uint64(0xaa4e0000),
		  uint64(0xdb1e42a6), uint64(0x256bbe15), uint64(0x123db156), uint64(0x3a4e99d7), },
		{ uint64(0x30b70000), uint64(0xe5d00000), uint64(0xf4f46000), uint64(0x42c40000),
		  uint64(0x63b83d6a), uint64(0x78ba9460), uint64(0x21afa1ea), uint64(0xb0a51834),
		  uint64(0xb6ce0000), uint64(0xdae90002), uint64(0x156e8000), uint64(0xda920000),
		  uint64(0xf6dd5a64), uint64(0x36325c8a), uint64(0xf272e8ae), uint64(0xa6b8c28d), },
		{ uint64(0x86790000), uint64(0x3f390002), uint64(0xe19ae000), uint64(0x98560000),
		  uint64(0x9565670e), uint64(0x4e88c8ea), uint64(0xd3dd4944), uint64(0x161ddab9),
		  uint64(0x30b70000), uint64(0xe5d00000), uint64(0xf4f46000), uint64(0x42c40000),
		  uint64(0x63b83d6a), uint64(0x78ba9460), uint64(0x21afa1ea), uint64(0xb0a51834), },
		{ uint64(0xdb250000), uint64(0x09290000), uint64(0x49aac000), uint64(0x81e10000),
		  uint64(0xcafe6b59), uint64(0x42793431), uint64(0x43566b76), uint64(0xe86cba2e),
		  uint64(0x75e60000), uint64(0x95660001), uint64(0x307b2000), uint64(0xadf40000),
		  uint64(0x8f321eea), uint64(0x24298307), uint64(0xe8c49cf9), uint64(0x4b7eec55), },
		{ uint64(0xaec30000), uint64(0x9c4f0001), uint64(0x79d1e000), uint64(0x2c150000),
		  uint64(0x45cc75b3), uint64(0x6650b736), uint64(0xab92f78f), uint64(0xa312567b),
		  uint64(0xdb250000), uint64(0x09290000), uint64(0x49aac000), uint64(0x81e10000),
		  uint64(0xcafe6b59), uint64(0x42793431), uint64(0x43566b76), uint64(0xe86cba2e), },
		{ uint64(0x1e4e0000), uint64(0xdecf0000), uint64(0x6df80180), uint64(0x77240000),
		  uint64(0xec47079e), uint64(0xf4a0694e), uint64(0xcda31812), uint64(0x98aa496e),
		  uint64(0xb2060000), uint64(0xc5690000), uint64(0x28031200), uint64(0x74670000),
		  uint64(0xb6c236f4), uint64(0xeb1239f8), uint64(0x33d1dfec), uint64(0x094e3198), },
		{ uint64(0xac480000), uint64(0x1ba60000), uint64(0x45fb1380), uint64(0x03430000),
		  uint64(0x5a85316a), uint64(0x1fb250b6), uint64(0xfe72c7fe), uint64(0x91e478f6),
		  uint64(0x1e4e0000), uint64(0xdecf0000), uint64(0x6df80180), uint64(0x77240000),
		  uint64(0xec47079e), uint64(0xf4a0694e), uint64(0xcda31812), uint64(0x98aa496e), },
		{ uint64(0x02af0000), uint64(0xb7280000), uint64(0xba1c0300), uint64(0x56980000),
		  uint64(0xba8d45d3), uint64(0x8048c667), uint64(0xa95c149a), uint64(0xf4f6ea7b),
		  uint64(0x7a8c0000), uint64(0xa5d40000), uint64(0x13260880), uint64(0xc63d0000),
		  uint64(0xcbb36daa), uint64(0xfea14f43), uint64(0x59d0b4f8), uint64(0x979961d0), },
		{ uint64(0x78230000), uint64(0x12fc0000), uint64(0xa93a0b80), uint64(0x90a50000),
		  uint64(0x713e2879), uint64(0x7ee98924), uint64(0xf08ca062), uint64(0x636f8bab),
		  uint64(0x02af0000), uint64(0xb7280000), uint64(0xba1c0300), uint64(0x56980000),
		  uint64(0xba8d45d3), uint64(0x8048c667), uint64(0xa95c149a), uint64(0xf4f6ea7b), },
		{ uint64(0x819e0000), uint64(0xec570000), uint64(0x66320280), uint64(0x95f30000),
		  uint64(0x5da92802), uint64(0x48f43cbc), uint64(0xe65aa22d), uint64(0x8e67b7fa),
		  uint64(0x4d8a0000), uint64(0x49340000), uint64(0x3c8b0500), uint64(0xaea30000),
		  uint64(0x16793bfd), uint64(0xcf6f08a4), uint64(0x8f19eaec), uint64(0x443d3004), },
		{ uint64(0xcc140000), uint64(0xa5630000), uint64(0x5ab90780), uint64(0x3b500000),
		  uint64(0x4bd013ff), uint64(0x879b3418), uint64(0x694348c1), uint64(0xca5a87fe),
		  uint64(0x819e0000), uint64(0xec570000), uint64(0x66320280), uint64(0x95f30000),
		  uint64(0x5da92802), uint64(0x48f43cbc), uint64(0xe65aa22d), uint64(0x8e67b7fa), },
		{ uint64(0x538d0000), uint64(0xa9fc0000), uint64(0x9ef70006), uint64(0x56ff0000),
		  uint64(0x0ae4004e), uint64(0x92c5cdf9), uint64(0xa9444018), uint64(0x7f975691),
		  uint64(0x01dd0000), uint64(0x80a80000), uint64(0xf4960048), uint64(0xa6000000),
		  uint64(0x90d57ea2), uint64(0xd7e68c37), uint64(0x6612cffd), uint64(0x2c94459e), },
		{ uint64(0x52500000), uint64(0x29540000), uint64(0x6a61004e), uint64(0xf0ff0000),
		  uint64(0x9a317eec), uint64(0x452341ce), uint64(0xcf568fe5), uint64(0x5303130f),
		  uint64(0x538d0000), uint64(0xa9fc0000), uint64(0x9ef70006), uint64(0x56ff0000),
		  uint64(0x0ae4004e), uint64(0x92c5cdf9), uint64(0xa9444018), uint64(0x7f975691), },
		{ uint64(0x0bc20000), uint64(0xdb630000), uint64(0x7e88000c), uint64(0x15860000),
		  uint64(0x91fd48f3), uint64(0x7581bb43), uint64(0xf460449e), uint64(0xd8b61463),
		  uint64(0x835a0000), uint64(0xc4f70000), uint64(0x01470022), uint64(0xeec80000),
		  uint64(0x60a54f69), uint64(0x142f2a24), uint64(0x5cf534f2), uint64(0x3ea660f7), },
		{ uint64(0x88980000), uint64(0x1f940000), uint64(0x7fcf002e), uint64(0xfb4e0000),
		  uint64(0xf158079a), uint64(0x61ae9167), uint64(0xa895706c), uint64(0xe6107494),
		  uint64(0x0bc20000), uint64(0xdb630000), uint64(0x7e88000c), uint64(0x15860000),
		  uint64(0x91fd48f3), uint64(0x7581bb43), uint64(0xf460449e), uint64(0xd8b61463), },
		{ uint64(0x07ed0000), uint64(0xb2500000), uint64(0x8774000a), uint64(0x970d0000),
		  uint64(0x437223ae), uint64(0x48c76ea4), uint64(0xf4786222), uint64(0x9075b1ce),
		  uint64(0xa2d60000), uint64(0xa6760000), uint64(0xc9440014), uint64(0xeba30000),
		  uint64(0xccec2e7b), uint64(0x3018c499), uint64(0x03490afa), uint64(0x9b6ef888), },
		{ uint64(0xa53b0000), uint64(0x14260000), uint64(0x4e30001e), uint64(0x7cae0000),
		  uint64(0x8f9e0dd5), uint64(0x78dfaa3d), uint64(0xf73168d8), uint64(0x0b1b4946),
		  uint64(0x07ed0000), uint64(0xb2500000), uint64(0x8774000a), uint64(0x970d0000),
		  uint64(0x437223ae), uint64(0x48c76ea4), uint64(0xf4786222), uint64(0x9075b1ce), },
		{ uint64(0x1d5a0000), uint64(0x2b720000), uint64(0x488d0000), uint64(0xaf611800),
		  uint64(0x25cb2ec5), uint64(0xc879bfd0), uint64(0x81a20429), uint64(0x1e7536a6),
		  uint64(0x45190000), uint64(0xab0c0000), uint64(0x30be0001), uint64(0x690a2000),
		  uint64(0xc2fc7219), uint64(0xb1d4800d), uint64(0x2dd1fa46), uint64(0x24314f17), },
		{ uint64(0x58430000), uint64(0x807e0000), uint64(0x78330001), uint64(0xc66b3800),
		  uint64(0xe7375cdc), uint64(0x79ad3fdd), uint64(0xac73fe6f), uint64(0x3a4479b1),
		  uint64(0x1d5a0000), uint64(0x2b720000), uint64(0x488d0000), uint64(0xaf611800),
		  uint64(0x25cb2ec5), uint64(0xc879bfd0), uint64(0x81a20429), uint64(0x1e7536a6), },
		{ uint64(0x92560000), uint64(0x1eda0000), uint64(0xea510000), uint64(0xe8b13000),
		  uint64(0xa93556a5), uint64(0xebfb6199), uint64(0xb15c2254), uint64(0x33c5244f),
		  uint64(0x8c3a0000), uint64(0xda980000), uint64(0x607f0000), uint64(0x54078800),
		  uint64(0x85714513), uint64(0x6006b243), uint64(0xdb50399c), uint64(0x8a58e6a4), },
		{ uint64(0x1e6c0000), uint64(0xc4420000), uint64(0x8a2e0000), uint64(0xbcb6b800),
		  uint64(0x2c4413b6), uint64(0x8bfdd3da), uint64(0x6a0c1bc8), uint64(0xb99dc2eb),
		  uint64(0x92560000), uint64(0x1eda0000), uint64(0xea510000), uint64(0xe8b13000),
		  uint64(0xa93556a5), uint64(0xebfb6199), uint64(0xb15c2254), uint64(0x33c5244f), },
		{ uint64(0xbadd0000), uint64(0x13ad0000), uint64(0xb7e70000), uint64(0xf7282800),
		  uint64(0xdf45144d), uint64(0x361ac33a), uint64(0xea5a8d14), uint64(0x2a2c18f0),
		  uint64(0xb82f0000), uint64(0xb12c0000), uint64(0x30d80000), uint64(0x14445000),
		  uint64(0xc15860a2), uint64(0x3127e8ec), uint64(0x2e98bf23), uint64(0x551e3d6e), },
		{ uint64(0x02f20000), uint64(0xa2810000), uint64(0x873f0000), uint64(0xe36c7800),
		  uint64(0x1e1d74ef), uint64(0x073d2bd6), uint64(0xc4c23237), uint64(0x7f32259e),
		  uint64(0xbadd0000), uint64(0x13ad0000), uint64(0xb7e70000), uint64(0xf7282800),
		  uint64(0xdf45144d), uint64(0x361ac33a), uint64(0xea5a8d14), uint64(0x2a2c18f0), },
		{ uint64(0xe3060000), uint64(0xbdc10000), uint64(0x87130000), uint64(0xbff20060),
		  uint64(0x2eba0a1a), uint64(0x8db53751), uint64(0x73c5ab06), uint64(0x5bd61539),
		  uint64(0x57370000), uint64(0xcaf20000), uint64(0x364e0000), uint64(0xc0220480),
		  uint64(0x56186b22), uint64(0x5ca3f40c), uint64(0xa1937f8f), uint64(0x15b961e7), },
		{ uint64(0xb4310000), uint64(0x77330000), uint64(0xb15d0000), uint64(0x7fd004e0),
		  uint64(0x78a26138), uint64(0xd116c35d), uint64(0xd256d489), uint64(0x4e6f74de),
		  uint64(0xe3060000), uint64(0xbdc10000), uint64(0x87130000), uint64(0xbff20060),
		  uint64(0x2eba0a1a), uint64(0x8db53751), uint64(0x73c5ab06), uint64(0x5bd61539), },
		{ uint64(0xf0c50000), uint64(0x59230000), uint64(0x45820000), uint64(0xe18d00c0),
		  uint64(0x3b6d0631), uint64(0xc2ed5699), uint64(0xcbe0fe1c), uint64(0x56a7b19f),
		  uint64(0x16ed0000), uint64(0x15680000), uint64(0xedd70000), uint64(0x325d0220),
		  uint64(0xe30c3689), uint64(0x5a4ae643), uint64(0xe375f8a8), uint64(0x81fdf908), },
		{ uint64(0xe6280000), uint64(0x4c4b0000), uint64(0xa8550000), uint64(0xd3d002e0),
		  uint64(0xd86130b8), uint64(0x98a7b0da), uint64(0x289506b4), uint64(0xd75a4897),
		  uint64(0xf0c50000), uint64(0x59230000), uint64(0x45820000), uint64(0xe18d00c0),
		  uint64(0x3b6d0631), uint64(0xc2ed5699), uint64(0xcbe0fe1c), uint64(0x56a7b19f), },
		{ uint64(0x7b280000), uint64(0x57420000), uint64(0xa9e50000), uint64(0x634300a0),
		  uint64(0x9edb442f), uint64(0x6d9995bb), uint64(0x27f83b03), uint64(0xc7ff60f0),
		  uint64(0x95bb0000), uint64(0x81450000), uint64(0x3b240000), uint64(0x48db0140),
		  uint64(0x0a8a6c53), uint64(0x56f56eec), uint64(0x62c91877), uint64(0xe7e00a94), },
		{ uint64(0xee930000), uint64(0xd6070000), uint64(0x92c10000), uint64(0x2b9801e0),
		  uint64(0x9451287c), uint64(0x3b6cfb57), uint64(0x45312374), uint64(0x201f6a64),
		  uint64(0x7b280000), uint64(0x57420000), uint64(0xa9e50000), uint64(0x634300a0),
		  uint64(0x9edb442f), uint64(0x6d9995bb), uint64(0x27f83b03), uint64(0xc7ff60f0), },
		{ uint64(0x00440000), uint64(0x7f480000), uint64(0xda7c0000), uint64(0x2a230001),
		  uint64(0x3badc9cc), uint64(0xa9b69c87), uint64(0x030a9e60), uint64(0xbe0a679e),
		  uint64(0x5fec0000), uint64(0x294b0000), uint64(0x99d20000), uint64(0x4ed00012),
		  uint64(0x1ed34f73), uint64(0xbaa708c9), uint64(0x57140bdf), uint64(0x30aebcf7), },
		{ uint64(0x5fa80000), uint64(0x56030000), uint64(0x43ae0000), uint64(0x64f30013),
		  uint64(0x257e86bf), uint64(0x1311944e), uint64(0x541e95bf), uint64(0x8ea4db69),
		  uint64(0x00440000), uint64(0x7f480000), uint64(0xda7c0000), uint64(0x2a230001),
		  uint64(0x3badc9cc), uint64(0xa9b69c87), uint64(0x030a9e60), uint64(0xbe0a679e), },
		{ uint64(0x92280000), uint64(0xdc850000), uint64(0x57fa0000), uint64(0x56dc0003),
		  uint64(0xbae92316), uint64(0x5aefa30c), uint64(0x90cef752), uint64(0x7b1675d7),
		  uint64(0x93bb0000), uint64(0x3b070000), uint64(0xba010000), uint64(0x99d00008),
		  uint64(0x3739ae4e), uint64(0xe64c1722), uint64(0x96f896b3), uint64(0x2879ebac), },
		{ uint64(0x01930000), uint64(0xe7820000), uint64(0xedfb0000), uint64(0xcf0c000b),
		  uint64(0x8dd08d58), uint64(0xbca3b42e), uint64(0x063661e1), uint64(0x536f9e7b),
		  uint64(0x92280000), uint64(0xdc850000), uint64(0x57fa0000), uint64(0x56dc0003),
		  uint64(0xbae92316), uint64(0x5aefa30c), uint64(0x90cef752), uint64(0x7b1675d7), },
		{ uint64(0xa8da0000), uint64(0x96be0000), uint64(0x5c1d0000), uint64(0x07da0002),
		  uint64(0x7d669583), uint64(0x1f98708a), uint64(0xbb668808), uint64(0xda878000),
		  uint64(0xabe70000), uint64(0x9e0d0000), uint64(0xaf270000), uint64(0x3d180005),
		  uint64(0x2c4f1fd3), uint64(0x74f61695), uint64(0xb5c347eb), uint64(0x3c5dfffe), },
		{ uint64(0x033d0000), uint64(0x08b30000), uint64(0xf33a0000), uint64(0x3ac20007),
		  uint64(0x51298a50), uint64(0x6b6e661f), uint64(0x0ea5cfe3), uint64(0xe6da7ffe),
		  uint64(0xa8da0000), uint64(0x96be0000), uint64(0x5c1d0000), uint64(0x07da0002),
		  uint64(0x7d669583), uint64(0x1f98708a), uint64(0xbb668808), uint64(0xda878000), },
		}


func New() hash.Digest {
	ref := &digest{}
	ref.Reset()
	return ref
}

func (ref *digest) Reset() {
	ref.partial, ref.bitCount, ref.partialLen = 0, 0, 0
	copy(ref.h[:], iv[:])
}

func makeT(x uint64) [][]uint64 {
	T := make([][]uint64, 256)

	for y := uint64(0); y < 256; y++ {
			if T[y] == nil {
				T[y] = make([]uint64, 16)
			}

			for z := uint64(0); z < 16; z++ {
				var a uint64
				for k := uint64(0); k < 8; k ++ {
					if ((y & (1 << (7 - k))) != 0) {
						a = a ^ tsrc[x + k][z]
					}
				}

				T[y][z] = a
			}
		}
	return T
}

func (ref *digest) Write(src []byte) (int, error) {

	var off int
	l := len(src)
	ref.bitCount += uint64(l) << 3

	if (ref.partialLen != 0) {
		for {
			if (ref.partialLen >= 8 && l <= 0) {
				break
			}
			ref.partial = (ref.partial << 8) | (uint64(src[off]) & 0xFF)
			ref.partialLen ++
			l--
			off++
		}
		if (ref.partialLen < 8) {
			return l, nil
		}
		ref.process((ref.partial >> 56) & 0xFF,
			(ref.partial >> 48) & 0xFF,
			(ref.partial >> 40) & 0xFF,
			(ref.partial >> 32) & 0xFF,
			(ref.partial >> 24) & 0xFF,
			(ref.partial >> 16) & 0xFF,
			(ref.partial >> 8) & 0xFF,
			(ref.partial) & 0xFF)
		ref.partialLen = 0
	}

	for {
		if l < 8 {
			break
		}
		ref.process(uint64(src[off + 0] & 0xFF),
			uint64(src[off + 1] & 0xFF),
			uint64(src[off + 2] & 0xFF),
			uint64(src[off + 3] & 0xFF),
			uint64(src[off + 4] & 0xFF),
			uint64(src[off + 5] & 0xFF),
			uint64(src[off + 6] & 0xFF),
			uint64(src[off + 7] & 0xFF))
		off += 8
		l -= 8
	}
	ref.partialLen = l
	for {
		l--
		if l <= 0 {
			break
		}

		ref.partial = (ref.partial << 8) | (uint64(src[off]) & 0xFF)
		off++
	}
	return l, nil
}

func (ref *digest) process(b0, b1, b2, b3, b4, b5, b6, b7 uint64) {
		var rp []uint64
		rp = t512_0[b0]
		m0 := rp[0x0]
		m1 := rp[0x1]
		m2 := rp[0x2]
		m3 := rp[0x3]
		m4 := rp[0x4]
		m5 := rp[0x5]
		m6 := rp[0x6]
		m7 := rp[0x7]
		m8 := rp[0x8]
		m9 := rp[0x9]
		mA := rp[0xA]
		mB := rp[0xB]
		mC := rp[0xC]
		mD := rp[0xD]
		mE := rp[0xE]
		mF := rp[0xF]
		rp = t512_1[b1]
		m0 ^= rp[0x0]
		m1 ^= rp[0x1]
		m2 ^= rp[0x2]
		m3 ^= rp[0x3]
		m4 ^= rp[0x4]
		m5 ^= rp[0x5]
		m6 ^= rp[0x6]
		m7 ^= rp[0x7]
		m8 ^= rp[0x8]
		m9 ^= rp[0x9]
		mA ^= rp[0xA]
		mB ^= rp[0xB]
		mC ^= rp[0xC]
		mD ^= rp[0xD]
		mE ^= rp[0xE]
		mF ^= rp[0xF]
		rp = t512_2[b2]
		m0 ^= rp[0x0]
		m1 ^= rp[0x1]
		m2 ^= rp[0x2]
		m3 ^= rp[0x3]
		m4 ^= rp[0x4]
		m5 ^= rp[0x5]
		m6 ^= rp[0x6]
		m7 ^= rp[0x7]
		m8 ^= rp[0x8]
		m9 ^= rp[0x9]
		mA ^= rp[0xA]
		mB ^= rp[0xB]
		mC ^= rp[0xC]
		mD ^= rp[0xD]
		mE ^= rp[0xE]
		mF ^= rp[0xF]
		rp = t512_3[b3]
		m0 ^= rp[0x0]
		m1 ^= rp[0x1]
		m2 ^= rp[0x2]
		m3 ^= rp[0x3]
		m4 ^= rp[0x4]
		m5 ^= rp[0x5]
		m6 ^= rp[0x6]
		m7 ^= rp[0x7]
		m8 ^= rp[0x8]
		m9 ^= rp[0x9]
		mA ^= rp[0xA]
		mB ^= rp[0xB]
		mC ^= rp[0xC]
		mD ^= rp[0xD]
		mE ^= rp[0xE]
		mF ^= rp[0xF]
		rp = t512_4[b4]
		m0 ^= rp[0x0]
		m1 ^= rp[0x1]
		m2 ^= rp[0x2]
		m3 ^= rp[0x3]
		m4 ^= rp[0x4]
		m5 ^= rp[0x5]
		m6 ^= rp[0x6]
		m7 ^= rp[0x7]
		m8 ^= rp[0x8]
		m9 ^= rp[0x9]
		mA ^= rp[0xA]
		mB ^= rp[0xB]
		mC ^= rp[0xC]
		mD ^= rp[0xD]
		mE ^= rp[0xE]
		mF ^= rp[0xF]
		rp = t512_5[b5]
		m0 ^= rp[0x0]
		m1 ^= rp[0x1]
		m2 ^= rp[0x2]
		m3 ^= rp[0x3]
		m4 ^= rp[0x4]
		m5 ^= rp[0x5]
		m6 ^= rp[0x6]
		m7 ^= rp[0x7]
		m8 ^= rp[0x8]
		m9 ^= rp[0x9]
		mA ^= rp[0xA]
		mB ^= rp[0xB]
		mC ^= rp[0xC]
		mD ^= rp[0xD]
		mE ^= rp[0xE]
		mF ^= rp[0xF]
		rp = t512_6[b6]
		m0 ^= rp[0x0]
		m1 ^= rp[0x1]
		m2 ^= rp[0x2]
		m3 ^= rp[0x3]
		m4 ^= rp[0x4]
		m5 ^= rp[0x5]
		m6 ^= rp[0x6]
		m7 ^= rp[0x7]
		m8 ^= rp[0x8]
		m9 ^= rp[0x9]
		mA ^= rp[0xA]
		mB ^= rp[0xB]
		mC ^= rp[0xC]
		mD ^= rp[0xD]
		mE ^= rp[0xE]
		mF ^= rp[0xF]
		rp = t512_7[b7]
		m0 ^= rp[0x0]
		m1 ^= rp[0x1]
		m2 ^= rp[0x2]
		m3 ^= rp[0x3]
		m4 ^= rp[0x4]
		m5 ^= rp[0x5]
		m6 ^= rp[0x6]
		m7 ^= rp[0x7]
		m8 ^= rp[0x8]
		m9 ^= rp[0x9]
		mA ^= rp[0xA]
		mB ^= rp[0xB]
		mC ^= rp[0xC]
		mD ^= rp[0xD]
		mE ^= rp[0xE]
		mF ^= rp[0xF]

		c0 := ref.h[0x0]
		c1 := ref.h[0x1]
		c2 := ref.h[0x2]
		c3 := ref.h[0x3]
		c4 := ref.h[0x4]
		c5 := ref.h[0x5]
		c6 := ref.h[0x6]
		c7 := ref.h[0x7]
		c8 := ref.h[0x8]
		c9 := ref.h[0x9]
		cA := ref.h[0xA]
		cB := ref.h[0xB]
		cC := ref.h[0xC]
		cD := ref.h[0xD]
		cE := ref.h[0xE]
		cF := ref.h[0xF]
		var t uint64

		for r := uint64(0); r < 6; r ++ {
			m0 ^= alpha_n[0x00]
			m1 ^= alpha_n[0x01] ^ r
			c0 ^= alpha_n[0x02]
			c1 ^= alpha_n[0x03]
			m2 ^= alpha_n[0x04]
			m3 ^= alpha_n[0x05]
			c2 ^= alpha_n[0x06]
			c3 ^= alpha_n[0x07]
			c4 ^= alpha_n[0x08]
			c5 ^= alpha_n[0x09]
			m4 ^= alpha_n[0x0A]
			m5 ^= alpha_n[0x0B]
			c6 ^= alpha_n[0x0C]
			c7 ^= alpha_n[0x0D]
			m6 ^= alpha_n[0x0E]
			m7 ^= alpha_n[0x0F]
			m8 ^= alpha_n[0x10]
			m9 ^= alpha_n[0x11]
			c8 ^= alpha_n[0x12]
			c9 ^= alpha_n[0x13]
			mA ^= alpha_n[0x14]
			mB ^= alpha_n[0x15]
			cA ^= alpha_n[0x16]
			cB ^= alpha_n[0x17]
			cC ^= alpha_n[0x18]
			cD ^= alpha_n[0x19]
			mC ^= alpha_n[0x1A]
			mD ^= alpha_n[0x1B]
			cE ^= alpha_n[0x1C]
			cF ^= alpha_n[0x1D]
			mE ^= alpha_n[0x1E]
			mF ^= alpha_n[0x1F]
			t = m0
			m0 &= m8
			m0 ^= cC
			m8 ^= c4
			m8 ^= m0
			cC |= t
			cC ^= c4
			t ^= m8
			c4 = cC
			cC |= t
			cC ^= m0
			m0 &= c4
			t ^= m0
			c4 ^= cC
			c4 ^= t
			m0 = m8
			m8 = c4
			c4 = cC
			cC = ^t
			t = m1
			m1 &= m9
			m1 ^= cD
			m9 ^= c5
			m9 ^= m1
			cD |= t
			cD ^= c5
			t ^= m9
			c5 = cD
			cD |= t
			cD ^= m1
			m1 &= c5
			t ^= m1
			c5 ^= cD
			c5 ^= t
			m1 = m9
			m9 = c5
			c5 = cD
			cD = ^t
			t = c0
			c0 &= c8
			c0 ^= mC
			c8 ^= m4
			c8 ^= c0
			mC |= t
			mC ^= m4
			t ^= c8
			m4 = mC
			mC |= t
			mC ^= c0
			c0 &= m4
			t ^= c0
			m4 ^= mC
			m4 ^= t
			c0 = c8
			c8 = m4
			m4 = mC
			mC = ^t
			t = c1
			c1 &= c9
			c1 ^= mD
			c9 ^= m5
			c9 ^= c1
			mD |= t
			mD ^= m5
			t ^= c9
			m5 = mD
			mD |= t
			mD ^= c1
			c1 &= m5
			t ^= c1
			m5 ^= mD
			m5 ^= t
			c1 = c9
			c9 = m5
			m5 = mD
			mD = ^t
			t = m2
			m2 &= mA
			m2 ^= cE
			mA ^= c6
			mA ^= m2
			cE |= t
			cE ^= c6
			t ^= mA
			c6 = cE
			cE |= t
			cE ^= m2
			m2 &= c6
			t ^= m2
			c6 ^= cE
			c6 ^= t
			m2 = mA
			mA = c6
			c6 = cE
			cE = ^t
			t = m3
			m3 &= mB
			m3 ^= cF
			mB ^= c7
			mB ^= m3
			cF |= t
			cF ^= c7
			t ^= mB
			c7 = cF
			cF |= t
			cF ^= m3
			m3 &= c7
			t ^= m3
			c7 ^= cF
			c7 ^= t
			m3 = mB
			mB = c7
			c7 = cF
			cF = ^t
			t = c2
			c2 &= cA
			c2 ^= mE
			cA ^= m6
			cA ^= c2
			mE |= t
			mE ^= m6
			t ^= cA
			m6 = mE
			mE |= t
			mE ^= c2
			c2 &= m6
			t ^= c2
			m6 ^= mE
			m6 ^= t
			c2 = cA
			cA = m6
			m6 = mE
			mE = ^t
			t = c3
			c3 &= cB
			c3 ^= mF
			cB ^= m7
			cB ^= c3
			mF |= t
			mF ^= m7
			t ^= cB
			m7 = mF
			mF |= t
			mF ^= c3
			c3 &= m7
			t ^= c3
			m7 ^= mF
			m7 ^= t
			c3 = cB
			cB = m7
			m7 = mF
			mF = ^t
			m0 = (m0 << 13) | (m0 >> (32 - 13))
			c8 = (c8 << 3) | (c8 >> (32 - 3))
			c5 ^= m0 ^ c8
			mD ^= c8 ^ (m0 << 3)
			c5 = (c5 << 1) | (c5 >> (32 - 1))
			mD = (mD << 7) | (mD >> (32 - 7))
			m0 ^= c5 ^ mD
			c8 ^= mD ^ (c5 << 7)
			m0 = (m0 << 5) | (m0 >> (32 - 5))
			c8 = (c8 << 22) | (c8 >> (32 - 22))
			m1 = (m1 << 13) | (m1 >> (32 - 13))
			c9 = (c9 << 3) | (c9 >> (32 - 3))
			m4 ^= m1 ^ c9
			cE ^= c9 ^ (m1 << 3)
			m4 = (m4 << 1) | (m4 >> (32 - 1))
			cE = (cE << 7) | (cE >> (32 - 7))
			m1 ^= m4 ^ cE
			c9 ^= cE ^ (m4 << 7)
			m1 = (m1 << 5) | (m1 >> (32 - 5))
			c9 = (c9 << 22) | (c9 >> (32 - 22))
			c0 = (c0 << 13) | (c0 >> (32 - 13))
			mA = (mA << 3) | (mA >> (32 - 3))
			m5 ^= c0 ^ mA
			cF ^= mA ^ (c0 << 3)
			m5 = (m5 << 1) | (m5 >> (32 - 1))
			cF = (cF << 7) | (cF >> (32 - 7))
			c0 ^= m5 ^ cF
			mA ^= cF ^ (m5 << 7)
			c0 = (c0 << 5) | (c0 >> (32 - 5))
			mA = (mA << 22) | (mA >> (32 - 22))
			c1 = (c1 << 13) | (c1 >> (32 - 13))
			mB = (mB << 3) | (mB >> (32 - 3))
			c6 ^= c1 ^ mB
			mE ^= mB ^ (c1 << 3)
			c6 = (c6 << 1) | (c6 >> (32 - 1))
			mE = (mE << 7) | (mE >> (32 - 7))
			c1 ^= c6 ^ mE
			mB ^= mE ^ (c6 << 7)
			c1 = (c1 << 5) | (c1 >> (32 - 5))
			mB = (mB << 22) | (mB >> (32 - 22))
			m2 = (m2 << 13) | (m2 >> (32 - 13))
			cA = (cA << 3) | (cA >> (32 - 3))
			c7 ^= m2 ^ cA
			mF ^= cA ^ (m2 << 3)
			c7 = (c7 << 1) | (c7 >> (32 - 1))
			mF = (mF << 7) | (mF >> (32 - 7))
			m2 ^= c7 ^ mF
			cA ^= mF ^ (c7 << 7)
			m2 = (m2 << 5) | (m2 >> (32 - 5))
			cA = (cA << 22) | (cA >> (32 - 22))
			m3 = (m3 << 13) | (m3 >> (32 - 13))
			cB = (cB << 3) | (cB >> (32 - 3))
			m6 ^= m3 ^ cB
			cC ^= cB ^ (m3 << 3)
			m6 = (m6 << 1) | (m6 >> (32 - 1))
			cC = (cC << 7) | (cC >> (32 - 7))
			m3 ^= m6 ^ cC
			cB ^= cC ^ (m6 << 7)
			m3 = (m3 << 5) | (m3 >> (32 - 5))
			cB = (cB << 22) | (cB >> (32 - 22))
			c2 = (c2 << 13) | (c2 >> (32 - 13))
			m8 = (m8 << 3) | (m8 >> (32 - 3))
			m7 ^= c2 ^ m8
			cD ^= m8 ^ (c2 << 3)
			m7 = (m7 << 1) | (m7 >> (32 - 1))
			cD = (cD << 7) | (cD >> (32 - 7))
			c2 ^= m7 ^ cD
			m8 ^= cD ^ (m7 << 7)
			c2 = (c2 << 5) | (c2 >> (32 - 5))
			m8 = (m8 << 22) | (m8 >> (32 - 22))
			c3 = (c3 << 13) | (c3 >> (32 - 13))
			m9 = (m9 << 3) | (m9 >> (32 - 3))
			c4 ^= c3 ^ m9
			mC ^= m9 ^ (c3 << 3)
			c4 = (c4 << 1) | (c4 >> (32 - 1))
			mC = (mC << 7) | (mC >> (32 - 7))
			c3 ^= c4 ^ mC
			m9 ^= mC ^ (c4 << 7)
			c3 = (c3 << 5) | (c3 >> (32 - 5))
			m9 = (m9 << 22) | (m9 >> (32 - 22))
			m0 = (m0 << 13) | (m0 >> (32 - 13))
			m3 = (m3 << 3) | (m3 >> (32 - 3))
			c0 ^= m0 ^ m3
			c3 ^= m3 ^ (m0 << 3)
			c0 = (c0 << 1) | (c0 >> (32 - 1))
			c3 = (c3 << 7) | (c3 >> (32 - 7))
			m0 ^= c0 ^ c3
			m3 ^= c3 ^ (c0 << 7)
			m0 = (m0 << 5) | (m0 >> (32 - 5))
			m3 = (m3 << 22) | (m3 >> (32 - 22))
			m8 = (m8 << 13) | (m8 >> (32 - 13))
			mB = (mB << 3) | (mB >> (32 - 3))
			c9 ^= m8 ^ mB
			cA ^= mB ^ (m8 << 3)
			c9 = (c9 << 1) | (c9 >> (32 - 1))
			cA = (cA << 7) | (cA >> (32 - 7))
			m8 ^= c9 ^ cA
			mB ^= cA ^ (c9 << 7)
			m8 = (m8 << 5) | (m8 >> (32 - 5))
			mB = (mB << 22) | (mB >> (32 - 22))
			c5 = (c5 << 13) | (c5 >> (32 - 13))
			c6 = (c6 << 3) | (c6 >> (32 - 3))
			m5 ^= c5 ^ c6
			m6 ^= c6 ^ (c5 << 3)
			m5 = (m5 << 1) | (m5 >> (32 - 1))
			m6 = (m6 << 7) | (m6 >> (32 - 7))
			c5 ^= m5 ^ m6
			c6 ^= m6 ^ (m5 << 7)
			c5 = (c5 << 5) | (c5 >> (32 - 5))
			c6 = (c6 << 22) | (c6 >> (32 - 22))
			cD = (cD << 13) | (cD >> (32 - 13))
			cE = (cE << 3) | (cE >> (32 - 3))
			mC ^= cD ^ cE
			mF ^= cE ^ (cD << 3)
			mC = (mC << 1) | (mC >> (32 - 1))
			mF = (mF << 7) | (mF >> (32 - 7))
			cD ^= mC ^ mF
			cE ^= mF ^ (mC << 7)
			cD = (cD << 5) | (cD >> (32 - 5))
			cE = (cE << 22) | (cE >> (32 - 22))
		}

		ref.h[0xF] ^= cB
		ref.h[0xE] ^= cA
		ref.h[0xD] ^= mB
		ref.h[0xC] ^= mA
		ref.h[0xB] ^= c9
		ref.h[0xA] ^= c8
		ref.h[0x9] ^= m9
		ref.h[0x8] ^= m8
		ref.h[0x7] ^= c3
		ref.h[0x6] ^= c2
		ref.h[0x5] ^= m3
		ref.h[0x4] ^= m2
		ref.h[0x3] ^= c1
		ref.h[0x2] ^= c0
		ref.h[0x1] ^= m1
		ref.h[0x0] ^= m0
	}

func (ref *digest) Close(dst []byte, bits uint8, bcnt uint8) error {


}

// Size returns the number of bytes required to store the hash.
func (*digest) Size() int {
	return HashSize
}

// BlockSize returns the block size of the hash.
func (*digest) BlockSize() int {
	return int(BlockSize)
}


var (
	t512_0 = makeT(0)
	t512_1 = makeT(8)
	t512_2 = makeT(16)
	t512_3 = makeT(24)
	t512_4 = makeT(32)
	t512_5 = makeT(40)
	t512_6 = makeT(48)
	t512_7 = makeT(56)
)

var alpha_n = [32]uint64{
		uint64(0xff00f0f0), uint64(0xccccaaaa), uint64(0xf0f0cccc), uint64(0xff00aaaa),
		uint64(0xccccaaaa), uint64(0xf0f0ff00), uint64(0xaaaacccc), uint64(0xf0f0ff00),
		uint64(0xf0f0cccc), uint64(0xaaaaff00), uint64(0xccccff00), uint64(0xaaaaf0f0),
		uint64(0xaaaaf0f0), uint64(0xff00cccc), uint64(0xccccf0f0), uint64(0xff00aaaa),
		uint64(0xccccaaaa), uint64(0xff00f0f0), uint64(0xff00aaaa), uint64(0xf0f0cccc),
		uint64(0xf0f0ff00), uint64(0xccccaaaa), uint64(0xf0f0ff00), uint64(0xaaaacccc),
		uint64(0xaaaaff00), uint64(0xf0f0cccc), uint64(0xaaaaf0f0), uint64(0xccccff00),
		uint64(0xff00cccc), uint64(0xaaaaf0f0), uint64(0xff00aaaa), uint64(0xccccf0f0),
	}

var alpha_f = [32]uint64{
		uint64(0xcaf9639c), uint64(0x0ff0f9c0), uint64(0x639c0ff0), uint64(0xcaf9f9c0),
		uint64(0x0ff0f9c0), uint64(0x639ccaf9), uint64(0xf9c00ff0), uint64(0x639ccaf9),
		uint64(0x639c0ff0), uint64(0xf9c0caf9), uint64(0x0ff0caf9), uint64(0xf9c0639c),
		uint64(0xf9c0639c), uint64(0xcaf90ff0), uint64(0x0ff0639c), uint64(0xcaf9f9c0),
		uint64(0x0ff0f9c0), uint64(0xcaf9639c), uint64(0xcaf9f9c0), uint64(0x639c0ff0),
		uint64(0x639ccaf9), uint64(0x0ff0f9c0), uint64(0x639ccaf9), uint64(0xf9c00ff0),
		uint64(0xf9c0caf9), uint64(0x639c0ff0), uint64(0xf9c0639c), uint64(0x0ff0caf9),
		uint64(0xcaf90ff0), uint64(0xf9c0639c), uint64(0xcaf9f9c0), uint64(0x0ff0639c),
	}
